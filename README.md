# README #

## Summary ##

This is a remake of the classic game, Missile Command but with a twist. The goal is to defend the planet from incoming missiles using the four satellites on-screen to shoot them down. The planet will visually be destroyed when hit and a life is lost when the core is exposed. The player's score is calculated after each wave.

## Contents of the project ##

This project contains the full visual studio solution, with full source code and resources included.

Also included in the project are various diagrams (Class, Game State) and an Introspective about the project.

## How do I get set up? ##

This application has been created and tested using Microsoft Visual Studio 2010 in Windows. Compatibility with other compilers and/or operating systems cannot be commented on.

Run the "Missile Command.sln" file found in "\Missile Command" to open the solution. From there, simply run the application to play the game.