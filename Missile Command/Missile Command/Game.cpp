#pragma once

#include "Game.h"

Game* Game::Get()
{
	static Game* TheGame = NULL;

	if (TheGame == NULL)
	{
		TheGame = new Game;
	}

	return TheGame;
}

void Game::Init()
{
	D3DVERTEXELEMENT9 PosColorVertexElements[] =
	{
		{ 0, 0, D3DDECLTYPE_FLOAT3, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_POSITION, 0 },
		{ 0, 3 * 4, D3DDECLTYPE_D3DCOLOR, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_COLOR, 0 },
		{ 0, 4 * 4, D3DDECLTYPE_FLOAT2, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_TEXCOORD, 0 },
		D3DDECL_END()
	};

	RenderManager::Get().g_pd3dDevice->CreateVertexDeclaration(PosColorVertexElements, &vertexDecleration);

	GameManager::Get()->StartNewGame();

	player.Init();

	planet.Init();

	missileSatellite[0].Init(Vector3(-200, -150, 0));
	missileSatellite[1].Init(Vector3(200, -150, 0));
	missileSatellite[2].Init(Vector3(-200, 150, 0));
	missileSatellite[3].Init(Vector3(200, 150, 0));
}

bool Game::Update()
{
	GameManager::Get()->Update();

	player.Update();

	planet.Update();

	missileSatellite[0].Update();
	missileSatellite[1].Update();
	missileSatellite[2].Update();
	missileSatellite[3].Update();


	// Update all Explosions in List
	std::list<Explosion>::iterator it_exp = explosionList.begin();

	while (it_exp != explosionList.end())
	{
		(*it_exp).Update();

		// Delete from list when animation has finished
		if ((*it_exp).HasExpired() == true)
		{
			it_exp = explosionList.erase(it_exp);
		}
		else
		{
			it_exp++;
		}
	}
	
	// Update all Player Missiles in List
	std::list<MissilePlayer>::iterator it_mis_p = missile_PlayerList.begin();
	
	while (it_mis_p != missile_PlayerList.end())
	{
		(*it_mis_p).Update();

		// Delete from list when it reaches its target and create a explosion
		if ((*it_mis_p).IsAtTarget() == true)
		{
			explosionList.push_back(Explosion::Explosion((*it_mis_p).position, true));
			it_mis_p = missile_PlayerList.erase(it_mis_p);
		}
		else
		{
			it_mis_p++;
		}
	}
	
	// Update all Enemy Missiles in List
	std::list<MissileEnemy>::iterator it_mis_e = missile_EnemyList.begin();

	while (it_mis_e != missile_EnemyList.end())
	{
		(*it_mis_e).Update(*it_mis_e);
		bool bExpired = false;

		std::list<Explosion>::iterator it_exp2 = explosionList.begin();

		while (it_exp2 != explosionList.end())
		{
			// If missile collides with and explosion that belongs to the player, set it as expired
			if (((*it_exp2).IsInMe((*it_mis_e)) == true) && ((*it_exp2).bBelongsToPlayer == true) && (bExpired == false))
			{
				bExpired = true;
				GameManager::Get()->score += 10;
			}

			it_exp2++;
		}

		// If the missile has not exploded yet
		if (bExpired == false)
		{
			// Check for a collision with the planet
			if ((planet).IsInMe(*it_mis_e) == true)
			{
				bExpired = true;

				if ((*it_mis_e).position.y < 0)
				{
					if ((*it_mis_e).position.x < 0)
						planet.ReduceHealth(Planet::TL);

					else
						planet.ReduceHealth(Planet::TR);
				}

				else
				{
					if ((*it_mis_e).position.x < 0)
						planet.ReduceHealth(Planet::BL);

					else
						planet.ReduceHealth(Planet::BR);
				}
			}
		}

		// If the missile has now exploded
		if (bExpired == true)
		{
			// Delete from list and create an explosion
			explosionList.push_back(Explosion::Explosion((*it_mis_e).position, false));
			it_mis_e = missile_EnemyList.erase(it_mis_e);
		}

		else
			it_mis_e++;
	}
	

	return false;
}

void Game::Draw()
{
	Matrix worldMatrix;
	Matrix viewMatrix;
	Matrix projectionMatrix;

	worldMatrix = Matrix::CreateRotationZ(0) * Matrix::CreateTranslation(RenderManager::Get().mWidth / 2.0f, RenderManager::Get().mHeight / 2.0f, 0);
	projectionMatrix = Matrix::CreateOrtho(0, RenderManager::Get().mWidth, RenderManager::Get().mHeight, 0, 0, 1);

	RenderManager::Get().g_pd3dDevice->SetTransform(D3DTS_VIEW, &viewMatrix);
	RenderManager::Get().g_pd3dDevice->SetTransform(D3DTS_WORLD, &worldMatrix);
	RenderManager::Get().g_pd3dDevice->SetTransform(D3DTS_PROJECTION, &projectionMatrix);

	RenderManager::Get().g_pd3dDevice->SetVertexDeclaration(vertexDecleration);
	RenderManager::Get().g_pd3dDevice->SetRenderState(D3DRS_LIGHTING, FALSE);
	RenderManager::Get().g_pd3dDevice->SetRenderState(D3DRS_CULLMODE, D3DCULL_NONE);

	planet.Draw();
	RenderManager::Get().g_pd3dDevice->SetVertexDeclaration(vertexDecleration);

	for (int i = 0; i < 4; i++)
	{
		missileSatellite[i].Draw();
		RenderManager::Get().g_pd3dDevice->SetVertexDeclaration(vertexDecleration);
	}

	for (std::list<MissileEnemy>::iterator it = missile_EnemyList.begin(); it != missile_EnemyList.end(); it++)
	{
		(*it).Draw();
		RenderManager::Get().g_pd3dDevice->SetVertexDeclaration(vertexDecleration);
	}

	for (std::list<MissilePlayer>::iterator it = missile_PlayerList.begin(); it != missile_PlayerList.end(); it++)
	{
		(*it).Draw();
		RenderManager::Get().g_pd3dDevice->SetVertexDeclaration(vertexDecleration);
	}

	for (std::list<Explosion>::iterator it = explosionList.begin(); it != explosionList.end(); it++)
	{
		(*it).Draw();
		RenderManager::Get().g_pd3dDevice->SetVertexDeclaration(vertexDecleration);
	}

	player.Draw(worldMatrix);
	RenderManager::Get().g_pd3dDevice->SetVertexDeclaration(vertexDecleration);
}

void Game::Exit()
{

}