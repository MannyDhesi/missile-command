#include "GameObject.h"


GameObject::GameObject()
{
	collider = NULL;
}

void GameObject::Init(const Vector3& location)
{
	position = location;
}

void GameObject::Update(){};

void GameObject::Draw()
{
	if (collider != NULL)
	{
		collider->Draw(D3DCOLOR_XRGB(0, 255, 0));
	}
}

// Check for a collision

bool GameObject::CollidesWith(const GameObject& ref)
{
	if (collider == NULL)	return false;

	return collider->Collides(*ref.collider);
}

bool GameObject::IsInMe(const GameObject& ref)
{
	if (collider == NULL)	return false;

	return collider->IsPointInMe(ref.position);
}