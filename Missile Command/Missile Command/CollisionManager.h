#pragma once

#include <vector>
#include <list>
//#include "BaseGame.h"
#include "RenderManager.h"

#define PI (3.1416f)

class ParametricLine
{
public:
	Vector3 v0, v1;

	ParametricLine(const Vector3& start, const Vector3& end);
	bool GetIntercept(const ParametricLine& line, Vector3& intercept) const;
};

class Collider
{
public:
	virtual void SetTransform(const Matrix& mat);
	virtual bool Collides(const Collider& object, std::list<Vector3>* collisionList = NULL);
	virtual bool IsPointInMe(const Vector3& pos);
	virtual void Draw(DWORD col = D3DCOLOR_XRGB(255, 255, 255));

protected:
	std::vector<Vector3> localPointList;
	std::vector<Vector3> worldPointList;
	std::vector<ParametricLine> lineList;
};

class PolyCollider : public Collider
{
public:
	PolyCollider();
	PolyCollider(const std::vector<Vector3>& edgeList);
	void Init(const std::vector<Vector3>& edgeList);
};

class CircleCollider : public PolyCollider
{
public:
	CircleCollider();
	CircleCollider(float radius, int segments = 16);
	void Init(float radius, int segments = 16);
};