#include "Explosion.h"

Explosion::Explosion(const Vector3 location, bool belongsToPlayer, float radius)
{
	position = location;

	lifetime = 50;
	currentTime = 0;

	maxRadius = radius;
	currentRadius = 0;

	sizeIncrease = maxRadius / (lifetime / 2);

	bBelongsToPlayer = belongsToPlayer;

	collider = new CircleCollider(currentRadius);
	collider->SetTransform(Matrix::CreateTranslation(position.x, position.y, position.z));
}

Explosion::~Explosion()
{

}

void Explosion::Update()
{
	// Create an animation by increasing the explosion size until the specified radius,
	// then decrease until it disappears

	if (currentTime <= lifetime)
	{
		if (currentTime < (lifetime / 2))
			currentRadius += sizeIncrease;

		else
			currentRadius -= sizeIncrease;

		currentTime++;
	}

	collider = new CircleCollider(currentRadius);
	collider->SetTransform(Matrix::CreateTranslation(position.x, position.y, position.z));
}

void Explosion::Draw()
{
	collider->Draw();
}

bool Explosion::HasExpired()
{
	return currentTime > lifetime;
}