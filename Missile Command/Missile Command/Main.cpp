#include "BaseGame.h"
#include "RenderManager.h"
#include "InputManager.h"
#include "ContentManager.h"

#include "Matrix.h"
#include "Vector3.h"
#include "Vector2.h"

#include "GameStates.h"
#include "GameStateManager.h"

class MyGame : public BaseGame
{
	public:
		
		void Init()
		{
			ShowCursor(false);

			ContentManager::Get().LoadFont("courier-18","courier",18, FW_REGULAR);
			ContentManager::Get().LoadFont("courier-32","courier",32, FW_REGULAR | FW_BOLD);

			GameStateManager::Get()->SetGameState(new GameState_MainMenu);
		}

		void Update()
		{
			GameStateManager::Get()->Update();
		}

		void Draw()
		{
			GameStateManager::Get()->Render();
		}

		void Exit()
		{

		}
};

//-----------------------------------------------------------------------------
// Name: wWinMain()
// Desc: The application's entry point
//-----------------------------------------------------------------------------
INT WINAPI wWinMain( HINSTANCE hInst, HINSTANCE, LPWSTR, INT )
{
	MyGame myGame;

	myGame.Run();

    
    return 0;
}

