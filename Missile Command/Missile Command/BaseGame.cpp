#include "BaseGame.h"

#include "RenderManager.h"
#include "InputManager.h"

#define SCREEN_X (800)
#define SCREEN_Y (600)


void BaseGame::Run()
{
	// Register the window class
	WNDCLASSEX wc =
	{
		sizeof( WNDCLASSEX ), CS_CLASSDC, MsgProc, 0L, 0L,
		GetModuleHandle( NULL ), NULL, NULL, NULL, NULL,
		L"Missile Command", NULL
	};
	RegisterClassEx( &wc );

	int WindowSpec = WS_OVERLAPPED | WS_CAPTION | WS_SYSMENU;
	
	SetRect(&mScreenRect, 0, 0, SCREEN_X, SCREEN_Y);
	AdjustWindowRectEx(&mScreenRect, WindowSpec, false, 0);


	// Create the application's window
	hWnd = CreateWindow( L"Missile Command", L"Missile Command"
		,WindowSpec
		,100, 100, mScreenRect.right - mScreenRect.left, mScreenRect.bottom - mScreenRect.top
		,NULL, NULL, wc.hInstance, NULL );

	// Initialize Direct3D
	if( SUCCEEDED( RenderManager::Get().InitD3D( hWnd,SCREEN_X,SCREEN_Y ) ) )
	{
		Init();

		// Show the window
		ShowWindow( hWnd, SW_SHOWDEFAULT );
		UpdateWindow( hWnd );

		// Enter the message loop
		MSG msg;
		ZeroMemory( &msg, sizeof( msg ) );
		while( msg.message != WM_QUIT )
		{
			if( PeekMessage( &msg, NULL, 0U, 0U, PM_REMOVE ) )
			{
				TranslateMessage( &msg );
				DispatchMessage( &msg );
			}
			else
			{
				InputManager::Get().Process();

				POINT cursorPos;
				GetCursorPos(&cursorPos);
				RECT aRect;
				GetWindowRect( hWnd, &aRect );

				cursorPos.x -= aRect.left - mScreenRect.left;
				cursorPos.y -= aRect.top - mScreenRect.top;

				InputManager::Get().SetMousePos(cursorPos.x,cursorPos.y);

				Update();

				RenderManager::Get().g_pd3dDevice->Clear( 0, NULL, D3DCLEAR_TARGET, D3DCOLOR_XRGB( 0, 0, 0 ), 1.0f, 0 );

				// Begin the scene
				if( SUCCEEDED( RenderManager::Get().g_pd3dDevice->BeginScene() ) )
				{
					Draw();

					RenderManager::Get().g_pd3dDevice->EndScene();
				}

				// Present the backbuffer contents to the display
				RenderManager::Get().g_pd3dDevice->Present( NULL, NULL, NULL, NULL );
			}
		}
	}

	Exit();

	

	RenderManager::Get().Cleanup();


	UnregisterClass( L"Missile Command", wc.hInstance );
}

//-----------------------------------------------------------------------------
// Name: MsgProc()
// Desc: The window's message handler
//-----------------------------------------------------------------------------
LRESULT WINAPI BaseGame::MsgProc( HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam )
{
	switch( msg )
	{
	case WM_DESTROY:
		PostQuitMessage( 0 );
		return 0;
	}

	return DefWindowProc( hWnd, msg, wParam, lParam );
}

