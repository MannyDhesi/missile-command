#pragma once

#include "MissileBase.h"

class MissileEnemy : public MissileBase
{
public:
	enum EnemyType
	{
		Single,
		Split,
	};
	
	MissileEnemy(const Vector3& _start, const Vector3& _target, float _speed, EnemyType type);
	~MissileEnemy();

	void Update(MissileEnemy& in);
	void Draw();

	EnemyType enemyType;

	float speed;
	float currentTime;
	float splitTime;
};

