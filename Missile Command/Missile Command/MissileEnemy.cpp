#include "MissileEnemy.h"
#include "Game.h"

MissileEnemy::MissileEnemy(const Vector3& _start, const Vector3& _target, float _speed, EnemyType type)
{
	start = _start;
	target = _target;
	speed = _speed;
	enemyType = type;

	currentTime = 0;
	splitTime = 30;

	Init(start, target, speed);
}

MissileEnemy::~MissileEnemy()
{

}

void MissileEnemy::Update(MissileEnemy& in)
{
	switch (enemyType)
	{
	case Single:
		break;

	case Split:
		// If the specified time to split has been reached
		if (currentTime == splitTime)
		{
			// Create a small explosion to visually show the split
			Game::Get()->explosionList.push_back(Explosion::Explosion(position, false, 10));
			
			// Set the new target for both missiles
			Vector3 target2;

			if (in.position.x < 0)
			{
				if (in.position.y < 0)
				{
					in.target = Vector3(0, -20, 0);
					target2 = Vector3(-20, 0, 0);
				}

				else
				{
					in.target = Vector3(0, 20, 0);
					target2 = Vector3(-20, 0, 0);
				}
			}

			else
			{
				if (in.position.y < 0)
				{
					in.target = Vector3(0, -20, 0);
					target2 = Vector3(20, 0, 0);
				}

				else
				{
					in.target = Vector3(0, 20, 0);
					target2 = Vector3(20, 0, 0);
				}
			}

			// Add a new missile to the list
			Game::Get()->missile_EnemyList.push_back(MissileEnemy::MissileEnemy(position, target2, speed, MissileEnemy::Single));
		}
		break;
	}

	currentTime++;

	MissileBase::Update();
}

void MissileEnemy::Draw()
{
	RenderManager::Get().DrawLine(start, position, D3DCOLOR_XRGB(255, 0, 0));

	collider->Draw(D3DCOLOR_XRGB(255, 0, 0));

	MissileBase::Draw();
}