#pragma once

#include "GameStateManager.h"

class GameState_MainMenu : public GameStateManager::State
{
public:
	void Init();
	bool Update();
	void Render();
	void Exit();

	const char* Name() { return "Main Menu"; }

	RECT r;
	wchar_t charArray[50];
};

class GameState_StartGame : public GameStateManager::State
{
public:
	void Init();
	bool Update();
	void Render();
	void Exit();

	const char* Name() { return "Start Game"; }
};

class GameState_StartWave : public GameStateManager::State
{
public:
	void Init();
	bool Update();
	void Render();
	void Exit();

	const char* Name() { return "Start Wave"; }

	int counter;
	int endCounter;

	RECT r;
	wchar_t charArray[50];
};

class GameState_Playing : public GameStateManager::State
{
public:
	void Init();
	bool Update();
	void Render();
	void Exit();

	const char* Name() { return "Playing"; }

	RECT r;
	wchar_t charArray[50];
};

class GameState_LostLife : public GameStateManager::State
{
public:
	void Init();
	bool Update();
	void Render();
	void Exit();

	const char* Name() { return "Lost Life"; }

	int counter;
	int endCounter;

	RECT r;
	wchar_t charArray[50];
};

class GameState_EndWave : public GameStateManager::State
{
public:
	void Init();
	bool Update();
	void Render();
	void Exit();

	const char* Name() { return "End Wave"; }

	int counter;
	int endCounter;

	RECT r;
	wchar_t charArray[50];
};

class GameState_GameOver : public GameStateManager::State
{
public:
	void Init();
	bool Update();
	void Render();
	void Exit();

	const char* Name() { return "Game Over"; }

	int counter;
	int endCounter;

	RECT r;
	wchar_t charArray[50];
};

class GameState_PauseGame : public GameStateManager::State
{
public:
	void Init();
	bool Update();
	void Render();
	void Exit();

	const char* Name() { return "Paused"; }

	RECT r;
	wchar_t charArray[50];
};