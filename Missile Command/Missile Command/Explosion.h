#pragma once

#include "GameObject.h"

class Explosion : public GameObject
{
public:
	Explosion(const Vector3 location, bool belongsToPlayer, float radius = 50);
	~Explosion();

	void Update();
	void Draw();

	bool HasExpired();

	float lifetime;
	float currentTime;

	float maxRadius;
	float currentRadius;

	float sizeIncrease;

	bool bBelongsToPlayer;
};

