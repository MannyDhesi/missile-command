#include "Planet.h"

void Planet::Init()
{
	GameObject::Init(Vector3(0, 0, 0));

	collider = new CircleCollider(80);
	collider->SetTransform(Matrix::CreateTranslation(0, 0, 0));

	position = Vector3(0, 0, 0);
	health_TL = 100;
	health_TR = 100;
	health_BL = 100;
	health_BR = 100;

	health_Average = (health_TL + health_TR + health_BL + health_BR) / 4;

	/* ----- Load All Textures ----- */

	// 100%
	ContentManager::Get().LoadTexture("planet_tl_100", "assets/textures/Planet/Planet - TL - 100.png");
	ContentManager::Get().LoadTexture("planet_tr_100", "assets/textures/Planet/Planet - TR - 100.png");
	ContentManager::Get().LoadTexture("planet_bl_100", "assets/textures/Planet/Planet - BL - 100.png");
	ContentManager::Get().LoadTexture("planet_br_100", "assets/textures/Planet/Planet - BR - 100.png");

	// 75%
	ContentManager::Get().LoadTexture("planet_tl_75", "assets/textures/Planet/Planet - TL - 75.png");
	ContentManager::Get().LoadTexture("planet_tr_75", "assets/textures/Planet/Planet - TR - 75.png");
	ContentManager::Get().LoadTexture("planet_bl_75", "assets/textures/Planet/Planet - BL - 75.png");
	ContentManager::Get().LoadTexture("planet_br_75", "assets/textures/Planet/Planet - BR - 75.png");

	// 50%
	ContentManager::Get().LoadTexture("planet_tl_50", "assets/textures/Planet/Planet - TL - 50.png");
	ContentManager::Get().LoadTexture("planet_tr_50", "assets/textures/Planet/Planet - TR - 50.png");
	ContentManager::Get().LoadTexture("planet_bl_50", "assets/textures/Planet/Planet - BL - 50.png");
	ContentManager::Get().LoadTexture("planet_br_50", "assets/textures/Planet/Planet - BR - 50.png");

	// 25%
	ContentManager::Get().LoadTexture("planet_tl_25", "assets/textures/Planet/Planet - TL - 25.png");
	ContentManager::Get().LoadTexture("planet_tr_25", "assets/textures/Planet/Planet - TR - 25.png");
	ContentManager::Get().LoadTexture("planet_bl_25", "assets/textures/Planet/Planet - BL - 25.png");
	ContentManager::Get().LoadTexture("planet_br_25", "assets/textures/Planet/Planet - BR - 25.png");

	// 0%
	ContentManager::Get().LoadTexture("planet_tl_0", "assets/textures/Planet/Planet - TL - 0.png");
	ContentManager::Get().LoadTexture("planet_tr_0", "assets/textures/Planet/Planet - TR - 0.png");
	ContentManager::Get().LoadTexture("planet_bl_0", "assets/textures/Planet/Planet - BL - 0.png");
	ContentManager::Get().LoadTexture("planet_br_0", "assets/textures/Planet/Planet - BR - 0.png");


	/* ----- Create the Vertices for each quarter ----- */

	// Create the vertices for the top-left quarter of the planet
	vertexList_Planet_TL[0].pos = position + Vector3( -100, -100, 0);		vertexList_Planet_TL[0].uv0 = Vector2(0, 0);
	vertexList_Planet_TL[1].pos = position + Vector3(    0, -100, 0);		vertexList_Planet_TL[1].uv0 = Vector2(1, 0);
	vertexList_Planet_TL[2].pos = position + Vector3( -100,    0, 0);		vertexList_Planet_TL[2].uv0 = Vector2(0, 1);

	vertexList_Planet_TL[3].pos = position + Vector3(    0, -100, 0);		vertexList_Planet_TL[3].uv0 = Vector2(1, 0);
	vertexList_Planet_TL[4].pos = position + Vector3(    0,    0, 0);		vertexList_Planet_TL[4].uv0 = Vector2(1, 1);
	vertexList_Planet_TL[5].pos = position + Vector3( -100,    0, 0);		vertexList_Planet_TL[5].uv0 = Vector2(0, 1);

	for (int i = 0; i < 6; i++)
		vertexList_Planet_TL[i].color = D3DCOLOR_XRGB(255, 255, 255);


	// Create the vertices for the top-right quarter of the planet
	vertexList_Planet_TR[0].pos = position + Vector3(    0, -100, 0);		vertexList_Planet_TR[0].uv0 = Vector2(0, 0);
	vertexList_Planet_TR[1].pos = position + Vector3(  100, -100, 0);		vertexList_Planet_TR[1].uv0 = Vector2(1, 0);
	vertexList_Planet_TR[2].pos = position + Vector3(    0,    0, 0);		vertexList_Planet_TR[2].uv0 = Vector2(0, 1);

	vertexList_Planet_TR[3].pos = position + Vector3(  100, -100, 0);		vertexList_Planet_TR[3].uv0 = Vector2(1, 0);
	vertexList_Planet_TR[4].pos = position + Vector3(  100,    0, 0);		vertexList_Planet_TR[4].uv0 = Vector2(1, 1);
	vertexList_Planet_TR[5].pos = position + Vector3(    0,    0, 0);		vertexList_Planet_TR[5].uv0 = Vector2(0, 1);

	for (int i = 0; i < 6; i++)
		vertexList_Planet_TR[i].color = D3DCOLOR_XRGB(255, 255, 255);


	// Create the vertices for the bottom-left quarter of the planet
	vertexList_Planet_BL[0].pos = position + Vector3( -100,    0, 0);		vertexList_Planet_BL[0].uv0 = Vector2(0, 0);
	vertexList_Planet_BL[1].pos = position + Vector3(    0,    0, 0);		vertexList_Planet_BL[1].uv0 = Vector2(1, 0);
	vertexList_Planet_BL[2].pos = position + Vector3( -100,  100, 0);		vertexList_Planet_BL[2].uv0 = Vector2(0, 1);

	vertexList_Planet_BL[3].pos = position + Vector3(    0,    0, 0);		vertexList_Planet_BL[3].uv0 = Vector2(1, 0);
	vertexList_Planet_BL[4].pos = position + Vector3(    0,  100, 0);		vertexList_Planet_BL[4].uv0 = Vector2(1, 1);
	vertexList_Planet_BL[5].pos = position + Vector3( -100,  100, 0);		vertexList_Planet_BL[5].uv0 = Vector2(0, 1);

	for (int i = 0; i < 6; i++)
		vertexList_Planet_BL[i].color = D3DCOLOR_XRGB(255, 255, 255);


	// Create the vertices for the bottom-right quarter of the planet
	vertexList_Planet_BR[0].pos = position + Vector3(    0,    0, 0);		vertexList_Planet_BR[0].uv0 = Vector2(0, 0);
	vertexList_Planet_BR[1].pos = position + Vector3(  100,    0, 0);		vertexList_Planet_BR[1].uv0 = Vector2(1, 0);
	vertexList_Planet_BR[2].pos = position + Vector3(    0,  100, 0);		vertexList_Planet_BR[2].uv0 = Vector2(0, 1);

	vertexList_Planet_BR[3].pos = position + Vector3(  100,    0, 0);		vertexList_Planet_BR[3].uv0 = Vector2(1, 0);
	vertexList_Planet_BR[4].pos = position + Vector3(  100,  100, 0);		vertexList_Planet_BR[4].uv0 = Vector2(1, 1);
	vertexList_Planet_BR[5].pos = position + Vector3(    0,  100, 0);		vertexList_Planet_BR[5].uv0 = Vector2(0, 1);

	for (int i = 0; i < 6; i++)
		vertexList_Planet_BR[i].color = D3DCOLOR_XRGB(255, 255, 255);
}

void Planet::Update()
{
	health_Average = (health_TL + health_TR + health_BL + health_BR) / 4;

	collider = new CircleCollider(80 * (health_Average / 100));
}

void Planet::Draw()
{
	// Draw the top-left quarter of the planet
	switch (health_TL)
	{
	case 100:
		RenderManager::Get().g_pd3dDevice->SetTexture(0, ContentManager::Get().GetContent("planet_tl_100")->GetTexture()->texture);
		break;

	case 75:
		RenderManager::Get().g_pd3dDevice->SetTexture(0, ContentManager::Get().GetContent("planet_tl_75")->GetTexture()->texture);
		break;

	case 50:
		RenderManager::Get().g_pd3dDevice->SetTexture(0, ContentManager::Get().GetContent("planet_tl_50")->GetTexture()->texture);
		break;

	case 25:
		RenderManager::Get().g_pd3dDevice->SetTexture(0, ContentManager::Get().GetContent("planet_tl_25")->GetTexture()->texture);
		break;

	case 0:
		RenderManager::Get().g_pd3dDevice->SetTexture(0, ContentManager::Get().GetContent("planet_tl_0")->GetTexture()->texture);
		break;
	}

	RenderManager::Get().g_pd3dDevice->DrawPrimitiveUP(D3DPT_TRIANGLELIST, 2, vertexList_Planet_TL, sizeof(CUSTOMVERTEX));
	RenderManager::Get().g_pd3dDevice->SetTexture(0, NULL);


	// Draw the top-right quarter of the planet
	switch (health_TR)
	{
	case 100:
		RenderManager::Get().g_pd3dDevice->SetTexture(0, ContentManager::Get().GetContent("planet_tr_100")->GetTexture()->texture);
		break;

	case 75:
		RenderManager::Get().g_pd3dDevice->SetTexture(0, ContentManager::Get().GetContent("planet_tr_75")->GetTexture()->texture);
		break;

	case 50:
		RenderManager::Get().g_pd3dDevice->SetTexture(0, ContentManager::Get().GetContent("planet_tr_50")->GetTexture()->texture);
		break;

	case 25:
		RenderManager::Get().g_pd3dDevice->SetTexture(0, ContentManager::Get().GetContent("planet_tr_25")->GetTexture()->texture);
		break;

	case 0:
		RenderManager::Get().g_pd3dDevice->SetTexture(0, ContentManager::Get().GetContent("planet_tr_0")->GetTexture()->texture);
		break;
	}

	RenderManager::Get().g_pd3dDevice->DrawPrimitiveUP(D3DPT_TRIANGLELIST, 2, vertexList_Planet_TR, sizeof(CUSTOMVERTEX));
	RenderManager::Get().g_pd3dDevice->SetTexture(0, NULL);


	// Draw the bottom-left quarter of the planet
	switch (health_BL)
	{
	case 100:
		RenderManager::Get().g_pd3dDevice->SetTexture(0, ContentManager::Get().GetContent("planet_bl_100")->GetTexture()->texture);
		break;

	case 75:
		RenderManager::Get().g_pd3dDevice->SetTexture(0, ContentManager::Get().GetContent("planet_bl_75")->GetTexture()->texture);
		break;

	case 50:
		RenderManager::Get().g_pd3dDevice->SetTexture(0, ContentManager::Get().GetContent("planet_bl_50")->GetTexture()->texture);
		break;

	case 25:
		RenderManager::Get().g_pd3dDevice->SetTexture(0, ContentManager::Get().GetContent("planet_bl_25")->GetTexture()->texture);
		break;

	case 0:
		RenderManager::Get().g_pd3dDevice->SetTexture(0, ContentManager::Get().GetContent("planet_bl_0")->GetTexture()->texture);
		break;
	}

	RenderManager::Get().g_pd3dDevice->DrawPrimitiveUP(D3DPT_TRIANGLELIST, 2, vertexList_Planet_BL, sizeof(CUSTOMVERTEX));
	RenderManager::Get().g_pd3dDevice->SetTexture(0, NULL);

	// Draw the bottom-right quarter of the planet
	switch (health_BR)
	{
	case 100:
		RenderManager::Get().g_pd3dDevice->SetTexture(0, ContentManager::Get().GetContent("planet_br_100")->GetTexture()->texture);
		break;

	case 75:
		RenderManager::Get().g_pd3dDevice->SetTexture(0, ContentManager::Get().GetContent("planet_br_75")->GetTexture()->texture);
		break;

	case 50:
		RenderManager::Get().g_pd3dDevice->SetTexture(0, ContentManager::Get().GetContent("planet_br_50")->GetTexture()->texture);
		break;

	case 25:
		RenderManager::Get().g_pd3dDevice->SetTexture(0, ContentManager::Get().GetContent("planet_br_25")->GetTexture()->texture);
		break;

	case 0:
		RenderManager::Get().g_pd3dDevice->SetTexture(0, ContentManager::Get().GetContent("planet_br_0")->GetTexture()->texture);
		break;
	}

	RenderManager::Get().g_pd3dDevice->DrawPrimitiveUP(D3DPT_TRIANGLELIST, 2, vertexList_Planet_BR, sizeof(CUSTOMVERTEX));
	RenderManager::Get().g_pd3dDevice->SetTexture(0, NULL);

	//collider->Draw();
}


void Planet::Exit()
{

}

void Planet::ReduceHealth(Planet::QUADRANTS quadrant)
{
	switch (quadrant)
	{
	case TL:
		health_TL -= 25;
		break;
	case TR:
		health_TR -= 25;
		break;
	case BL:
		health_BL -= 25;
		break;
	case BR:
		health_BR -= 25;
		break;
	}

	if (health_TL <= 0)
		health_TL = 0;

	if (health_TR <= 0)
		health_TR = 0;

	if (health_BL <= 0)
		health_BL = 0;

	if (health_BR <= 0)
		health_BR = 0;
}

void Planet::ResetHealth()
{
	health_TL = 100;
	health_TR = 100;
	health_BL = 100;
	health_BR = 100;
}