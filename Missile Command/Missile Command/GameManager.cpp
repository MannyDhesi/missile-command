#include "GameManager.h"

GameManager* GameManager::Get()
{
	static GameManager *pSingleton = NULL;

	if (pSingleton == NULL)
	{
		pSingleton = new GameManager;
	}

	return pSingleton;
}

void GameManager::Update()
{
	// Spawn a new enemy after the specified time has passed
	if (lastSpawnCounter >= spawnTime && currentEnemiesInWave < totalEnemiesInWave)
	{
		SpawnEnemy();
		currentEnemiesInWave++;
		lastSpawnCounter = 0;
	}

	lastSpawnCounter++;
}

void GameManager::StartNewGame()
{
	// Reset variables when a new game is started
	score = 0;
	lives = 3;
	wave = 0;

	bGameOver = false;

	currentEnemiesInWave = 0;
	totalEnemiesInWave = 10;
	spawnTime = 100;
	lastSpawnCounter = 0;

	StartNextWave();
}

void GameManager::LoseLife()
{
	// Deduct a life and  check if it is game over
	lives -= 1;

	if (lives <= 0)
	{
		lives = 0;
		bGameOver = true;
	}
}

void GameManager::StartNextWave()
{
	// Set variables for next wave
	wave += 1;
	totalEnemiesInWave = 6 + (wave * 2);
}

void GameManager::InitWave()
{
	// Initialise the current wave
	ClearScreen();

	currentEnemiesInWave = 0;
	lastSpawnCounter = spawnTime;
	Game::Get()->planet.ResetHealth();
}

void GameManager::SpawnEnemy()
{
	// Spawn enemy at random position and velocity

	int x, y;
	int entrySide = rand() % 4;
	
	switch (entrySide)
	{
	case 0:
		x = -400 + rand() % 800;
		y = -300;
		break;
	case 1:
		x = 400;
		y = -300 + rand() % 600;
		break;
	case 2:
		x = -400 + rand() % 800;
		y = 300;
		break;
	case 3:
		x = -400;
		y = -300 + rand() % 600;
		break;
	}

	Vector3 location = Vector3(x, y, 0);

	// Enemy missiles have a higher speed at higher waves
	float speed = 1.5 + ((wave - 1) * 0.25f);
	if (speed > 3)
		speed = 3;

	// When the player reaches wave 3, 'Split' missiles are introduces

	if (wave < 3)
		Game::Get()->missile_EnemyList.push_back(MissileEnemy::MissileEnemy(location, Game::Get()->planet.position, speed, MissileEnemy::Single));

	else
	{
		// 1 in 5 chance of a split missile spawning
		int enemyType = rand() % 5;

		switch (enemyType)
		{
		case 0:
		case 1:
		case 2:
		case 3:
			Game::Get()->missile_EnemyList.push_back(MissileEnemy::MissileEnemy(location, Game::Get()->planet.position, speed, MissileEnemy::Single));
			break;

		case 4:
			Game::Get()->missile_EnemyList.push_back(MissileEnemy::MissileEnemy(location, Game::Get()->planet.position, speed, MissileEnemy::Split));
			break;
		}
	}
}

bool GameManager::HasWaveFinished()
{
	return ((currentEnemiesInWave == totalEnemiesInWave) && 
		(Game::Get()->missile_EnemyList.size() == 0) && 
		(Game::Get()->missile_PlayerList.size() == 0) && 
		(Game::Get()->explosionList.size() == 0));
}

void GameManager::ClearScreen()
{
	Game::Get()->missile_PlayerList.clear();
	Game::Get()->missile_EnemyList.clear();
	Game::Get()->explosionList.clear();
}