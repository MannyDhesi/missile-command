#pragma once

#include "BaseGame.h"
#include "RenderManager.h"
#include "ContentManager.h"

#include "GameObject.h"

#include "Matrix.h"
#include "Vector3.h"
#include "Vector2.h"

class MissileBase : public GameObject
{
public:
	void Init(const Vector3 start, const Vector3 target, float speed);
	void Update();
	void Draw();

	Vector3 start, target, velocity;
};

