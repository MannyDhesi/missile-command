#pragma once

#include "BaseGame.h"
#include "RenderManager.h"
#include "ContentManager.h"

#include "GameObject.h"

#include "Matrix.h"
#include "Vector3.h"
#include "Vector2.h"

class MissileSatellites : public GameObject
{
public:
	void Init(const Vector3 location);
	void Update();
	void Draw();
	
	bool CanShoot();
	void ShootMissile(Vector3& target);

	struct CUSTOMVERTEX
	{
		Vector3 pos;      // The untransformed, 3D position for the vertex
		DWORD color;        // The vertex color
		Vector2 uv0;
	};

private:
	CUSTOMVERTEX vertexList[6];
};

