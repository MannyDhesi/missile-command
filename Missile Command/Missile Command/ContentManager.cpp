#include "ContentManager.h"
#include "RenderManager.h"

bool ContentManager::LoadTexture(const char* Name, const char* Filename)
{
	if(GetContent(Name) == false)
	{
		TextureContent *pContent = new TextureContent();

		D3DXCreateTextureFromFileExA(RenderManager::Get().g_pd3dDevice
			,Filename
			,D3DX_DEFAULT_NONPOW2
			,D3DX_DEFAULT_NONPOW2
			,1
			,0
			,D3DFMT_UNKNOWN
			,D3DPOOL_MANAGED
			,D3DX_DEFAULT
			,D3DX_DEFAULT
			,0
			,&pContent->mRef.info
			,NULL							   
			,&pContent->mRef.texture);

		if(pContent->mRef.texture!=NULL)
		{
			TheContentDictionary[Name] = pContent;
			return true;
		}

		delete pContent;

	}
	return false;
}
bool ContentManager::LoadWAV(const char* Name, const char* Filename)
{
	return false;
}

bool ContentManager::LoadFont(const char* Name,char* FontName, int size, int flags)
{
	if(GetContent(Name) == false)
	{
		FontContent *pContent = new FontContent();


		D3DXCreateFontA(RenderManager::Get().g_pd3dDevice
			, size
			, 0
			, flags
			, 0
			, FALSE
			, DEFAULT_CHARSET
			, OUT_TT_ONLY_PRECIS
			, DEFAULT_QUALITY
			, DEFAULT_PITCH | FF_DONTCARE
			, FontName
			, &pContent->mRef.font );

		if(pContent->mRef.font != NULL)
		{
			TheContentDictionary[Name] = pContent;
			return true;
		}

		delete pContent;
	}	
	return false;
}

ContentManager::BaseContent* ContentManager::GetContent(const char* name)
{
	ContentDictionary::iterator it = TheContentDictionary.find(name);

	if(it == TheContentDictionary.end())
	{
		return false;
	}

	return it->second;
}