#include "CollisionManager.h"

ParametricLine::ParametricLine(const Vector3& start, const Vector3& end)
{
	v0 = start;
	v1 = end;
}

bool ParametricLine::GetIntercept(const ParametricLine& line, Vector3& intercept) const
{
	intercept = Vector3(0, 0, 0);

	float a = v0.x;
	float c = v1.x - v0.x;
	float d = line.v0.x;
	float f = line.v1.x - line.v0.x;
	float g = v0.y;
	float h = v1.y - v0.y;
	float i = line.v0.y;
	float j = line.v1.y - line.v0.y;
	float k = ((j * c) - (f * h));

	if (fabs(k) < 0.001f)	return false;

	float t = ((j * (d - a)) + (f * (g - i))) / k;

	if (t > 1 || t < 0) return false;

	intercept.x = v0.x + t * (v1.x - v0.x);
	intercept.y = v0.y + t * (v1.y - v0.y);

	if (fabs(line.v1.x - line.v0.x) > 0)
	{
		t = (intercept.x - line.v0.x) / (line.v1.x - line.v0.x);
	}
	else
	{
		t = (intercept.y - line.v0.x) / (line.v1.x - line.v0.x);
	}

	if (t > 1 || t < 0) return false;

	return true;
}



void Collider::SetTransform(const Matrix& mat)
{
	lineList.clear();

	worldPointList.clear();

	for (unsigned int i = 0; i< localPointList.size(); i += 2)
	{
		Vector3 v0 = localPointList[i];
		Vector3 v1 = localPointList[i + 1];

		v0 = v0.Transform(v0, mat);
		v1 = v1.Transform(v1, mat);
		lineList.push_back(ParametricLine(v0, v1));

		worldPointList.push_back(v0);
		worldPointList.push_back(v1);
	}
}

bool Collider::Collides(const Collider& object, std::list<Vector3>* collisionList)
{
	bool bGotCollision = false;
	for (std::vector<ParametricLine>::const_iterator paramLine1 = object.lineList.begin(); paramLine1 != object.lineList.end(); paramLine1++)
	{
		for (std::vector<ParametricLine>::const_iterator paramLine2 = lineList.begin(); paramLine2 != lineList.end(); paramLine2++)
		{
			Vector3 result;

			if (paramLine1->GetIntercept((*paramLine2), result))
			{
				if (collisionList == NULL)
				{
					return true;
				}

				collisionList->push_back(result);

				bGotCollision = true;
			}
		}
	}
	return bGotCollision;
}

bool Collider::IsPointInMe(const Vector3& pos)
{
	unsigned int i, j = worldPointList.size() - 1;
	bool oddNodes = false;

	for (i = 0; i < worldPointList.size(); i++)
	{
		if ((worldPointList[i].y < pos.y && worldPointList[j].y >= pos.y)
			|| (worldPointList[j].y < pos.y && worldPointList[i].y >= pos.y)
			)
		{
			if (worldPointList[i].x + (pos.y - worldPointList[i].y) / (worldPointList[j].y - worldPointList[i].y) * (worldPointList[j].x - worldPointList[i].x) < pos.x)
			{
				oddNodes = !oddNodes;
			}
		}
		j = i;
	}

	return oddNodes;
}

void Collider::Draw(DWORD col)
{
	for (unsigned int i = 0; i< localPointList.size(); i += 2)
	{
		Vector3 v0 = worldPointList[i];
		Vector3 v1 = worldPointList[i + 1];

		RenderManager::Get().DrawLine(v0, v1, col);
	}
}


PolyCollider::PolyCollider() {}

PolyCollider::PolyCollider(const std::vector<Vector3>& edgeList)
{ 
	Init(edgeList); 
}

void PolyCollider::Init(const std::vector<Vector3>& edgeList)
{
	localPointList.clear();

	Vector3 v1, v2;

	std::vector<Vector3>::const_iterator it = edgeList.begin();

	v1 = (*it);
	it++;

	while (it != edgeList.end())
	{
		v2 = (*it);
		localPointList.push_back(v1);
		localPointList.push_back(v2);

		v1 = v2;
		it++;
	}

	it = edgeList.begin();

	v1 = (*it);

	localPointList.push_back(v1);
	localPointList.push_back(v2);

	SetTransform(Matrix::Identity);
}


CircleCollider::CircleCollider() {}

CircleCollider::CircleCollider(float radius, int segments)	
{ 
	Init(radius, segments); 
}

void CircleCollider::Init(float radius, int segments)
{
	std::vector<Vector3> polyList;

	for (int i = 0; i<segments; i++)
	{
		Vector3 rad(radius, 0, 0);
		Matrix rot = Matrix::CreateRotationZ((2 * PI*i) / segments);

		polyList.push_back(rad.Transform(rad, rot));
	}

	PolyCollider::Init(polyList);
}