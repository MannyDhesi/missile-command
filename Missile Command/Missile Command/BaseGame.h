#pragma once
#pragma warning( disable : 4996 ) // disable deprecated warning 
#pragma warning( disable : 4995 ) // disable deprecated warning 
#pragma warning( disable : 4244 ) // disable deprecated warning 

#include "Windows.h"
#include <d3d9.h>
#include <strsafe.h>
#include <d3dx9core.h>



class BaseGame
{
	public:
		void Run();

		virtual void Init(){};
		virtual void Update(){};
		virtual void Draw(){};
		virtual void Exit(){};
		
		HWND hWnd;
		RECT mScreenRect;

		static LRESULT WINAPI MsgProc( HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam );
		VOID Cleanup();
};