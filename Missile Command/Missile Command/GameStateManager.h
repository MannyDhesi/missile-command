#pragma once
#include "Singleton.h"
#include "RenderManager.h"
#include "InputManager.h"
#include "ContentManager.h"

class GameStateManager : Singleton<GameStateManager>
{
	public:
		class State
		{
			public:
				virtual void Init()=0;
				virtual bool Update() { return false; }
				virtual void Render()=0;
				virtual void Exit()=0;

				virtual const char* Name()
				{
					return "No label defined";
				}
		};
	private:
		State* TheGameState;
		State* TheNextGameState;
		
		GameStateManager() 
		{ 
			TheGameState = TheNextGameState = NULL;
		}

		void Init()
		{
			if (TheGameState != NULL)
			{
				TheGameState->Init();
			}
			else
			{
				if (TheNextGameState != NULL)
				{
					TheGameState = TheNextGameState;
					TheNextGameState = NULL;
				}

				TheGameState->Init();
			}
		}

	public:

		static GameStateManager* Get()
		{
			static GameStateManager* TheGameStateManager=NULL;

			if (TheGameStateManager == NULL)
			{
				TheGameStateManager = new GameStateManager();
			}

			return TheGameStateManager;
		}
		

		void SetGameState(State* NewState)
		{
			TheNextGameState = NewState;
		}

		bool Update()
		{
			if (TheNextGameState != NULL)
			{
				if(TheGameState != NULL)
				{
					delete TheGameState;
				}

				TheGameState = TheNextGameState;
				TheNextGameState = NULL;

				TheGameState->Init();
			}

			if(InputManager::Get().GetKey(VK_ESCAPE) == true)
			{
				return false;
			}

			if (TheGameState == NULL)
			{
				return true;
			}
			return TheGameState->Update();
		}

		void Render()
		{
			if (TheGameState != NULL)
			{
				TheGameState->Render();

				RECT r;
				r.left = 10; r.top = 10; r.right = 800; r.bottom = 480;

				if (InputManager::Get().IsKeyHeld(VK_F1) == true)
				{
					ContentManager::Get().GetContent("courier-18")->GetFont()->font->DrawTextA(NULL, "Current State: ", -1, &r, 0, D3DCOLOR_XRGB(255, 255, 255));
					r.top = 30;
					ContentManager::Get().GetContent("courier-18")->GetFont()->font->DrawTextA(NULL, TheGameState->Name(), -1, &r, 0, D3DCOLOR_XRGB(255, 255, 255));
				}
			}
		}
};
