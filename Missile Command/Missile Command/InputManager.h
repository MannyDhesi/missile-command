#pragma once
#include "Windows.h"

#include "Singleton.h"

class InputManager : public Singleton<InputManager>
{
	public:
		friend class BaseGame;

		struct KeyState
		{
			KeyState()
			{
				bPressedLastFrame = bPressedThisFrame = false;
			}
			bool bPressedThisFrame;
			bool bPressedLastFrame;
		};

		KeyState myKeyState[255];

		D3DXVECTOR2 mCurrentPos;
		D3DXVECTOR2 mOldPos;


		bool GetKey(char i)
		{
			return myKeyState[i].bPressedThisFrame;
		}

		bool GetOldKey(char i)
		{
			return myKeyState[i].bPressedLastFrame;
		}
		
		bool IsKeyPressed(char i)
		{
			return GetKey(i) && !GetOldKey(i);
		}

		bool IsKeyHeld(char i)
		{
			return GetKey(i) && GetOldKey(i);
		}

		bool IsKeyReleased(char i)
		{
			return !GetKey(i) && GetOldKey(i);
		}

		bool IsKeyNotPressed(char i)
		{
			return !GetKey(i) && !GetOldKey(i);
		}

		D3DXVECTOR2 GetMousePos()
		{
			return mCurrentPos;
		}

		D3DXVECTOR2 GetMouseOldPos()
		{
			return mOldPos;
		}

	private:

		void Process()
		{
			for(int i =0;i<255;i++)
			{
				myKeyState[i].bPressedLastFrame = myKeyState[i].bPressedThisFrame;

				myKeyState[i].bPressedThisFrame = GetAsyncKeyState(i)?true:false;				
			}			
		}

		void SetMousePos(int x, int y)
		{
			mOldPos = mCurrentPos;
			mCurrentPos.x = (float)x;
			mCurrentPos.y = (float)y;
		}

};