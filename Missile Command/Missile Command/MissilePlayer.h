#pragma once

#include "MissileBase.h"

class MissilePlayer : public MissileBase
{
public:
	MissilePlayer(const Vector3& _start, const Vector3& _target);
	~MissilePlayer();

	void Update();
	void Draw();

	bool IsAtTarget();
};

