#pragma once

#include "CollisionManager.h"

#include "Vector3.h"
#include "Vector2.h"

class GameObject
{
public:
	GameObject();
	
	virtual void Init(const Vector3& location);
	virtual void Update();
	virtual void Draw();

	virtual bool CollidesWith(const GameObject& ref);
	virtual bool IsInMe(const GameObject& ref);

	Vector3 position;
	Vector3 oldPosition;

	Collider *collider;
};

