#include "MissileSatellites.h"
#include "Game.h"

void MissileSatellites::Init(const Vector3 location)
{
	GameObject::Init(location);

	ContentManager::Get().LoadTexture("missile_satellite", "assets/textures/MissileSatellite.png");

	// Create the vertices
	vertexList[0].pos = position + Vector3( -50, -50, 0);		vertexList[0].uv0 = Vector2(0, 0);
	vertexList[1].pos = position + Vector3(  50, -50, 0);		vertexList[1].uv0 = Vector2(1, 0);
	vertexList[2].pos = position + Vector3( -50,  50, 0);		vertexList[2].uv0 = Vector2(0, 1);

	vertexList[3].pos = position + Vector3(  50, -50, 0);		vertexList[3].uv0 = Vector2(1, 0);
	vertexList[4].pos = position + Vector3(  50,  50, 0);		vertexList[4].uv0 = Vector2(1, 1);
	vertexList[5].pos = position + Vector3( -50,  50, 0);		vertexList[5].uv0 = Vector2(0, 1);

	for (int i = 0; i < 6; i++)
		vertexList[i].color = D3DCOLOR_XRGB(255, 255, 255);

	collider = new CircleCollider(50);
	collider->SetTransform(Matrix::CreateTranslation(location.x, location.y, location.z));
}

void MissileSatellites::Update()
{

}

void MissileSatellites::Draw()
{
	// Draw the vertices
	vertexList[0].pos = position + Vector3( -50, -50, 0);		vertexList[0].uv0 = Vector2(0, 0);
	vertexList[1].pos = position + Vector3(  50, -50, 0);		vertexList[1].uv0 = Vector2(1, 0);
	vertexList[2].pos = position + Vector3( -50,  50, 0);		vertexList[2].uv0 = Vector2(0, 1);

	vertexList[3].pos = position + Vector3(  50, -50, 0);		vertexList[3].uv0 = Vector2(1, 0);
	vertexList[4].pos = position + Vector3(  50,  50, 0);		vertexList[4].uv0 = Vector2(1, 1);
	vertexList[5].pos = position + Vector3( -50,  50, 0);		vertexList[5].uv0 = Vector2(0, 1);

	for (int i = 0; i < 6; i++)
		vertexList[i].color = D3DCOLOR_XRGB(255, 255, 255);

	// Apply texture
	RenderManager::Get().g_pd3dDevice->SetTexture(0, ContentManager::Get().GetContent("missile_satellite")->GetTexture()->texture);
	RenderManager::Get().g_pd3dDevice->DrawPrimitiveUP(D3DPT_TRIANGLELIST, 2, vertexList, sizeof(CUSTOMVERTEX));
	RenderManager::Get().g_pd3dDevice->SetTexture(0, NULL);
}


bool MissileSatellites::CanShoot()
{
	// Check if the satellites can shoot
	if (GameManager::Get()->currentEnemiesInWave == GameManager::Get()->totalEnemiesInWave &&
		Game::Get()->missile_EnemyList.size() == 0)
		return false;

	else return true;
}

void MissileSatellites::ShootMissile(Vector3& target)
{
	// Add a Player Missile to the List
	Game::Get()->missile_PlayerList.push_back(MissilePlayer(position, target));
}