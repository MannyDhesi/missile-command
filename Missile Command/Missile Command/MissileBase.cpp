#include "MissileBase.h"

void MissileBase::Init(const Vector3 start, const Vector3 target, float speed)
{
	GameObject::Init(start);

	velocity = target - start;
	velocity = velocity.Normalize(velocity, start, target);
	velocity *= speed;

	collider = new CircleCollider(2);
	collider->SetTransform(Matrix::CreateTranslation(position.x, position.y, position.z));
}

void MissileBase::Update()
{
	position += velocity;

	collider->SetTransform(Matrix::CreateTranslation(position.x, position.y, position.z));
}

void MissileBase::Draw()
{

}