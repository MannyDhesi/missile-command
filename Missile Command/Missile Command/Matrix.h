#pragma once
#include <d3dx9math.h>

class Matrix : public D3DXMATRIXA16
{
	public:

		Matrix();

		static const Matrix Identity;

		static Matrix CreateTranslation(float x, float y, float z)
		{
			Matrix mat;
			D3DXMatrixTranslation(&mat,x,y,z);

			return mat;
		}

		static Matrix CreateOrtho(float l, float r, float b, float t, float n, float f)
		{
			Matrix mat;
			D3DXMatrixOrthoOffCenterRH(&mat,l,r,b,t,n,f);

			return mat;
		}

		static Matrix CreateRotationZ(float rad)
		{
			Matrix mat;
			D3DXMatrixRotationZ(&mat,rad);

			return mat;
		}

		static Matrix Multiply(const Matrix& a, const Matrix& b)
		{
			Matrix mat;
			D3DXMatrixMultiply(&mat,&a,&b);
			return mat;
		}

		void operator =(const D3DXMATRIX &ref)
		{
			memcpy(*this,ref,sizeof(D3DXMATRIX));
		}

};