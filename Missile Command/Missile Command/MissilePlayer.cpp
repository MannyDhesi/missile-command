#include "MissilePlayer.h"

MissilePlayer::MissilePlayer(const Vector3& _start, const Vector3& _target)
{
	start = _start;
	target = _target;

	Init(start, target, 5);
}

MissilePlayer::~MissilePlayer()
{

}

void MissilePlayer::Update()
{
	MissileBase::Update();
}

void MissilePlayer::Draw()
{
	RenderManager::Get().DrawLine(start, position, D3DCOLOR_XRGB(0, 0, 255));

	collider->Draw(D3DCOLOR_XRGB(0, 0, 255));

	MissileBase::Draw();
}

bool MissilePlayer::IsAtTarget()
{
	return Vector3::Distance(target, position) < velocity.Length(velocity);
}