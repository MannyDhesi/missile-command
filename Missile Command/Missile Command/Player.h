#pragma once

#include "BaseGame.h"
#include "RenderManager.h"
#include "InputManager.h"
#include "ContentManager.h"

#include "GameObject.h"

#include "Matrix.h"
#include "Vector3.h"
#include "Vector2.h"

class Player : public GameObject
{
public:
	void Init();
	void Update();
	void Draw(Matrix& worldMat);
	void Exit();

	bool bCanShoot;
};

