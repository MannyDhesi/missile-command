#include "RenderManager.h"

HRESULT RenderManager::InitD3D( HWND hWnd, int screenWidth, int screenHeight )
{
	mWidth = screenWidth;
	mHeight = screenHeight;

	// Create the D3D object.
	if( NULL == ( g_pD3D = Direct3DCreate9( D3D_SDK_VERSION ) ) )
		return E_FAIL;

	// Set up the structure used to create the D3DDevice
	D3DPRESENT_PARAMETERS d3dpp;
	ZeroMemory( &d3dpp, sizeof( d3dpp ) );
	d3dpp.Windowed = TRUE;
	d3dpp.SwapEffect = D3DSWAPEFFECT_DISCARD;
	d3dpp.BackBufferFormat = D3DFMT_UNKNOWN;

	// Create the D3DDevice
	if( FAILED( g_pD3D->CreateDevice( D3DADAPTER_DEFAULT, D3DDEVTYPE_HAL, hWnd,
		D3DCREATE_SOFTWARE_VERTEXPROCESSING,
		&d3dpp, &g_pd3dDevice ) ) )
	{
		return E_FAIL;
	}

	InitDebugRenderer();

	return S_OK;
}


VOID RenderManager::Cleanup()
{
	if( g_pd3dDevice != NULL )
		g_pd3dDevice->Release();

	if( g_pD3D != NULL )
		g_pD3D->Release();
}


void RenderManager::InitDebugRenderer()
{
	D3DVERTEXELEMENT9 PosColorVertexElements[] = 
	{
		{0,   0, D3DDECLTYPE_FLOAT3,     D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_POSITION,  0},
		{0, 3*4, D3DDECLTYPE_D3DCOLOR,   D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_COLOR,     0},
		D3DDECL_END()
	};

	g_pd3dDevice->CreateVertexDeclaration(PosColorVertexElements,&vertexDecleration);

}

void RenderManager::DebugRenderTris(const DEBUGVERTEX* verts, int triCount)
{	
	g_pd3dDevice->SetVertexDeclaration(vertexDecleration);
	g_pd3dDevice->DrawPrimitiveUP(D3DPT_TRIANGLELIST,triCount,verts,sizeof(DEBUGVERTEX) );
}

void RenderManager::DrawLine(const Vector3& start, const Vector3& end, DWORD colour)
{
	DEBUGVERTEX lineVertices[2];

	const DWORD line_fvf = D3DFVF_XYZ | D3DFVF_DIFFUSE;

	lineVertices[0].Pos = start;
	lineVertices[1].Pos = end;

	lineVertices[0].Color = colour;
	lineVertices[1].Color = colour;

	RenderManager::Get().g_pd3dDevice->SetFVF(line_fvf);

	RenderManager::Get().g_pd3dDevice->DrawPrimitiveUP(D3DPT_LINELIST, 1, lineVertices, sizeof(DEBUGVERTEX));
}