#pragma once

#include "BaseGame.h"
#include "RenderManager.h"
#include "ContentManager.h"

#include "GameObject.h"

#include "Matrix.h"
#include "Vector3.h"
#include "Vector2.h"

class Planet : public GameObject
{
public:
	void Init();
	void Update();
	void Draw();
	void Exit();

	enum QUADRANTS
	{
		TL,
		TR,
		BL,
		BR
	};

	void ReduceHealth(QUADRANTS quadrant);
	void ResetHealth();

	struct CUSTOMVERTEX
	{
		Vector3 pos;      // The untransformed, 3D position for the vertex
		DWORD color;        // The vertex color
		Vector2 uv0;
	};

	int health_TL;
	int health_TR;
	int health_BL;
	int health_BR;

	float health_Average;

private:
	CUSTOMVERTEX vertexList_Planet_TL[6];
	CUSTOMVERTEX vertexList_Planet_TR[6];
	CUSTOMVERTEX vertexList_Planet_BL[6];
	CUSTOMVERTEX vertexList_Planet_BR[6];
};

