#pragma once
#include <d3dx9math.h>

class Vector2 : public D3DXVECTOR2
{
	public:
		Vector2(){};
		Vector2(float _x,float _y)
		{
			x = _x;
			y = _y;
		};
		
		static Vector2 Transform(const Vector2& in, const Matrix& mat)
		{
			D3DXVECTOR4 out;

			D3DXVec2Transform(&out,&in,&mat);

			return Vector2(out.x,out.y);
		}
};