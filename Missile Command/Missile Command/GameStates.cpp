#include "GameStates.h"
#include "Game.h"

/********************
Main Menu
********************/

void GameState_MainMenu::Init()
{
	GameManager::Get()->ClearScreen();
	Game::Get()->Init();
}

bool GameState_MainMenu::Update()
{
	if (InputManager::Get().IsKeyReleased(VK_SPACE))
	{
		GameStateManager::Get()->SetGameState(new GameState_StartGame);
	}

	if (InputManager::Get().IsKeyReleased(VK_ESCAPE))
	{
		exit(0);
	}

	return State::Update();
}

void GameState_MainMenu::Render()
{
	Game::Get()->Draw();

	r.left = 0; r.top = 100; r.right = 800; r.bottom = 150;
	wsprintfW(charArray, L"Missile Command");
	ContentManager::Get().GetContent("courier-32")->GetFont()->font->DrawText(NULL, charArray, -1, &r, DT_CENTER, D3DCOLOR_XRGB(255, 255, 255));

	r.left = 0; r.top = 135; r.right = 800; r.bottom = 800;
	wsprintfW(charArray, L"Created by Manpreet Dhesi");
	ContentManager::Get().GetContent("courier-18")->GetFont()->font->DrawText(NULL, charArray, -1, &r, DT_CENTER, D3DCOLOR_XRGB(255, 255, 255));

	r.left = 0; r.top = 500; r.right = 800; r.bottom = 800;
	wsprintfW(charArray, L"Press Space To Play!");
	ContentManager::Get().GetContent("courier-32")->GetFont()->font->DrawText(NULL, charArray, -1, &r, DT_CENTER, D3DCOLOR_XRGB(255, 255, 255));
}

void GameState_MainMenu::Exit()
{

}



/********************
Start Game
********************/

void GameState_StartGame::Init()
{
	Game::Get()->Init();
}

bool GameState_StartGame::Update()
{
	GameStateManager::Get()->SetGameState(new GameState_StartWave);

	return true;
}
void GameState_StartGame::Render()
{
	Game::Get()->Draw();
}

void GameState_StartGame::Exit()
{

}



/********************
Start Wave
********************/

void GameState_StartWave::Init()
{
	counter = 0;
	endCounter = 60;

	GameManager::Get()->InitWave();
}

bool GameState_StartWave::Update()
{
	if (counter >= endCounter)
		GameStateManager::Get()->SetGameState(new GameState_Playing);

	else
		counter++;

	return State::Update();
}
void GameState_StartWave::Render()
{
	Game::Get()->Draw();

	r.left = 0; r.top = 100; r.right = 800; r.bottom = 480;
	wsprintfW(charArray, L"Wave %d", GameManager::Get()->wave);
	ContentManager::Get().GetContent("courier-32")->GetFont()->font->DrawText(NULL, charArray, -1, &r, DT_CENTER, D3DCOLOR_XRGB(255, 255, 255));

	r.left = 0; r.top = 135; r.right = 800; r.bottom = 480;
	wsprintfW(charArray, L"Get Ready");
	ContentManager::Get().GetContent("courier-18")->GetFont()->font->DrawText(NULL, charArray, -1, &r, DT_CENTER, D3DCOLOR_XRGB(255, 255, 255));
}

void GameState_StartWave::Exit()
{

}



/********************
Playing
********************/

void GameState_Playing::Init()
{

}

bool GameState_Playing::Update()
{
	Game::Get()->Update();

	if (Game::Get()->planet.health_TL == 0 ||
		Game::Get()->planet.health_TR == 0 ||
		Game::Get()->planet.health_BL == 0 ||
		Game::Get()->planet.health_BR == 0)
	{
		GameStateManager::Get()->SetGameState(new GameState_LostLife);
	}

	if (GameManager::Get()->HasWaveFinished() == true)
		GameStateManager::Get()->SetGameState(new GameState_EndWave);

	if (InputManager::Get().IsKeyReleased(0x50))
		GameStateManager::Get()->SetGameState(new GameState_PauseGame);

	return State::Update();
}
void GameState_Playing::Render()
{
	Game::Get()->Draw();

	r.left = 0; r.top = 10; r.right = 400; r.bottom = 100;
	wsprintfW(charArray, L"Wave: %d", GameManager::Get()->wave);
	ContentManager::Get().GetContent("courier-18")->GetFont()->font->DrawText(NULL, charArray, -1, &r, DT_CENTER, D3DCOLOR_XRGB(255, 255, 255));

	r.left = 200; r.top = 10; r.right = 600; r.bottom = 100;
	wsprintfW(charArray, L"Score: %d", GameManager::Get()->score);
	ContentManager::Get().GetContent("courier-18")->GetFont()->font->DrawText(NULL, charArray, -1, &r, DT_CENTER, D3DCOLOR_XRGB(255, 255, 255));

	r.left = 400; r.top = 10; r.right = 800; r.bottom = 100;
	wsprintfW(charArray, L"Lives: %d", GameManager::Get()->lives);
	ContentManager::Get().GetContent("courier-18")->GetFont()->font->DrawText(NULL, charArray, -1, &r, DT_CENTER, D3DCOLOR_XRGB(255, 255, 255));
}

void GameState_Playing::Exit()
{

}



/********************
Lost Life
********************/

void GameState_LostLife::Init()
{
	counter = 0;
	endCounter = 60;

	GameManager::Get()->LoseLife();
}

bool GameState_LostLife::Update()
{
	if (counter >= endCounter)
	{
		if (GameManager::Get()->bGameOver == false)
		{
			GameStateManager::Get()->SetGameState(new GameState_StartWave);
		}

		else
			GameStateManager::Get()->SetGameState(new GameState_GameOver);
	}

	else
		counter++;

	return State::Update();
}
void GameState_LostLife::Render()
{
	Game::Get()->Draw();

	r.left = 0; r.top = 100; r.right = 800; r.bottom = 480;
	wsprintfW(charArray, L"Core Exposed!\n%d Lives Remaining!", GameManager::Get()->lives);
	ContentManager::Get().GetContent("courier-32")->GetFont()->font->DrawText(NULL, charArray, -1, &r, DT_CENTER, D3DCOLOR_XRGB(255, 255, 255));
}

void GameState_LostLife::Exit()
{

}



/********************
End Wave
********************/

void GameState_EndWave::Init()
{
	counter = 0;
	endCounter = 160;
}

bool GameState_EndWave::Update()
{
	if (counter == (endCounter * 0.65))
		GameManager::Get()->score += Game::Get()->planet.health_Average;

	if (counter == (endCounter * 0.7))
		GameManager::Get()->score += Game::Get()->planet.health_Average;

	if (counter == (endCounter * 0.75))
		GameManager::Get()->score += (GameManager::Get()->lives * 50);
	
	if (counter >= endCounter)
	{
		GameStateManager::Get()->SetGameState(new GameState_StartWave);
		GameManager::Get()->StartNextWave();
	}

	else
		counter++;

	return State::Update();
}
void GameState_EndWave::Render()
{
	Game::Get()->Draw();

	r.left = 0; r.top = 100; r.right = 800; r.bottom = 150;
	wsprintfW(charArray, L"End of Wave %d", GameManager::Get()->wave);
	ContentManager::Get().GetContent("courier-32")->GetFont()->font->DrawText(NULL, charArray, -1, &r, DT_CENTER, D3DCOLOR_XRGB(255, 255, 255));

	if (counter > (endCounter * 0.2))
	{
		r.left = 0; r.top = 135; r.right = 800; r.bottom = 480;
		wsprintfW(charArray, L"Planet Health = %d %%", (int)Game::Get()->planet.health_Average);
		ContentManager::Get().GetContent("courier-18")->GetFont()->font->DrawText(NULL, charArray, -1, &r, DT_CENTER, D3DCOLOR_XRGB(255, 255, 255));
	}

	if (counter > (endCounter * 0.4))
	{
		r.left = 0; r.top = 155; r.right = 800; r.bottom = 480;
		wsprintfW(charArray, L"Lives = %d", GameManager::Get()->lives);
		ContentManager::Get().GetContent("courier-18")->GetFont()->font->DrawText(NULL, charArray, -1, &r, DT_CENTER, D3DCOLOR_XRGB(255, 255, 255));
	}

	if (counter > (endCounter * 0.6))
	{
		r.left = 0; r.top = 175; r.right = 800; r.bottom = 480;
		wsprintfW(charArray, L"Score = %d", GameManager::Get()->score);
		ContentManager::Get().GetContent("courier-18")->GetFont()->font->DrawText(NULL, charArray, -1, &r, DT_CENTER, D3DCOLOR_XRGB(255, 255, 255));
	}
}

void GameState_EndWave::Exit()
{

}



/********************
Game Over
********************/

void GameState_GameOver::Init()
{
	counter = 0;
	endCounter = 60;
}

bool GameState_GameOver::Update()
{
	if (counter >= endCounter)
		GameStateManager::Get()->SetGameState(new GameState_MainMenu);

	else
		counter++;

	return State::Update();
}
void GameState_GameOver::Render()
{
	Game::Get()->Draw();

	r.left = 0; r.top = 100; r.right = 800; r.bottom = 480;
	wsprintfW(charArray, L"Game Over!");
	ContentManager::Get().GetContent("courier-32")->GetFont()->font->DrawText(NULL, charArray, -1, &r, DT_CENTER, D3DCOLOR_XRGB(255, 255, 255));

	r.left = 0; r.top = 135; r.right = 800; r.bottom = 480;
	wsprintfW(charArray, L"Final Score: %d", GameManager::Get()->score);
	ContentManager::Get().GetContent("courier-18")->GetFont()->font->DrawText(NULL, charArray, -1, &r, DT_CENTER, D3DCOLOR_XRGB(255, 255, 255));
}

void GameState_GameOver::Exit()
{

}



/********************
Pause Game
********************/

void GameState_PauseGame::Init()
{

}

bool GameState_PauseGame::Update()
{
	if (InputManager::Get().IsKeyReleased(0x50))
		GameStateManager::Get()->SetGameState(new GameState_Playing);

	if (InputManager::Get().IsKeyReleased(VK_ESCAPE))
		GameStateManager::Get()->SetGameState(new GameState_MainMenu);

	return State::Update();
}
void GameState_PauseGame::Render()
{
	Game::Get()->Draw();

	r.left = 0; r.top = 100; r.right = 800; r.bottom = 480;
	wsprintfW(charArray, L"Game Paused");
	ContentManager::Get().GetContent("courier-32")->GetFont()->font->DrawText(NULL, charArray, -1, &r, DT_CENTER, D3DCOLOR_XRGB(255, 255, 255));

	r.left = 0; r.top = 135; r.right = 800; r.bottom = 480;
	wsprintfW(charArray, L"P = Return to game\nEsc = Quit");
	ContentManager::Get().GetContent("courier-18")->GetFont()->font->DrawText(NULL, charArray, -1, &r, DT_CENTER, D3DCOLOR_XRGB(255, 255, 255));
}

void GameState_PauseGame::Exit()
{

}