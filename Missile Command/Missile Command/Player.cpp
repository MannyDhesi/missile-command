#include "Player.h"
#include "Game.h"

void Player::Init()
{
	position = Vector3(InputManager::Get().GetMousePos().x, InputManager::Get().GetMousePos().y, 0);

	bCanShoot = true;
}

void Player::Update()
{
	// Get the mouse position
	oldPosition = position;
	position = Vector3(InputManager::Get().GetMousePos().x, InputManager::Get().GetMousePos().y, 0);
	
	// If the player presses shoot button and they can shoot
	if (InputManager::Get().IsKeyReleased(VK_LBUTTON) && bCanShoot == true)
	{
		if (Game::Get()->missileSatellite[0].CanShoot() == true ||
			Game::Get()->missileSatellite[1].CanShoot() == true ||
			Game::Get()->missileSatellite[2].CanShoot() == true ||
			Game::Get()->missileSatellite[3].CanShoot() == true)
		{
			// Get mouse posiition in world space
			Vector3 target = position;
			target += Vector3(-400, -300, 0);

			// Find the nearest satellite to the mouse position
			if (target.x > -400 && target.x < 400 && target.y > -300 && target.y < 300)
			{
				int nearestSatellite = 0;
				float distance = 9999;

				for (int i = 0; i < 4; i++)
				{
					if (Vector3::Distance(Game::Get()->missileSatellite[i].position, target) < distance)
					{
						distance = Vector3::Distance(Game::Get()->missileSatellite[i].position, target);
						nearestSatellite = i;
					}
				}

				// Shoot missile
				Game::Get()->missileSatellite[nearestSatellite].ShootMissile(target);
			}
		}
	}
}

void Player::Draw(Matrix& worldMat)
{
	// Draw mouse crosshair
	D3DXVECTOR2 pos = InputManager::Get().GetMousePos();
	D3DXMatrixTranslation(&worldMat, pos.x, pos.y, 0);

	// Draw the image
	RenderManager::Get().g_pd3dDevice->SetTransform(D3DTS_WORLD, &worldMat);
	RenderManager::Get().DrawLine(Vector3(0, -10, 0), Vector3(0, 10, 0), D3DCOLOR_XRGB(0, 0, 255));
	RenderManager::Get().DrawLine(Vector3(-10, 0, 0), Vector3(10, 0, 0), D3DCOLOR_XRGB(0, 0, 255));
}

void Player::Exit()
{

}