#pragma once

#include "Singleton.h"

#include "Game.h"

class GameManager : Singleton<GameManager>
{
public:
	static GameManager* Get();

	void Update();

	void StartNewGame();
	void LoseLife();
	void StartNextWave();
	void InitWave();
	void SpawnEnemy();
	bool HasWaveFinished();
	void ClearScreen();

	int score;
	int lives;
	int wave;

	bool bGameOver;

	int currentEnemiesInWave;
	int totalEnemiesInWave;
	int spawnTime;
	int lastSpawnCounter;
};

