#pragma once
#include "Singleton.h"

#include <d3d9.h>
#pragma warning( disable : 4996 ) // disable deprecated warning 
#include <strsafe.h>
#pragma warning( default : 4996 )

#include <d3dx9core.h>

#include "Matrix.h"
#include "Vector3.h"


class RenderManager : public Singleton<RenderManager>
{
	public:
		int mWidth,mHeight;

		HRESULT InitD3D( HWND hWnd, int screenWidth, int screenHeight );
		VOID Cleanup();

		LPDIRECT3D9             g_pD3D;
		LPDIRECT3DDEVICE9       g_pd3dDevice;


		struct DEBUGVERTEX
		{
			DEBUGVERTEX(){};
			DEBUGVERTEX(const Vector3& pos, DWORD col)
			{
				Pos = pos;
				Color = col;
			}

			Vector3 Pos;      // The untransformed, 3D position for the vertex
			DWORD Color;        // The vertex color
		};

		void DebugRenderTris(const DEBUGVERTEX* verts, int triCount);

		void DrawLine(const Vector3& start, const Vector3& end, DWORD colour);

	private:
		IDirect3DVertexDeclaration9* vertexDecleration;

		void InitDebugRenderer();
};
