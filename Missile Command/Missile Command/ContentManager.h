#pragma once
#include "Singleton.h"
#include "BaseGame.h"

#include <map>

class ContentManager : public Singleton<ContentManager>
{
	public:

		class TextureRef
		{
			public:
				TextureRef()
				{
					Reset();
				}
			
				LPDIRECT3DTEXTURE9 texture;
				D3DXIMAGE_INFO info;

			private:
				void Reset()
				{
					texture = NULL;
				}
		};

		class WavRef
		{
			public:
		};

		class FontRef
		{
			public:
				ID3DXFont* font;

		};

		class BaseContent
		{
			public:

				virtual TextureRef* GetTexture(){return NULL;}
				virtual WavRef*		GetWAV(){return NULL;}
				virtual FontRef*	GetFont(){return NULL;}				
		};

		class TextureContent : public BaseContent
		{
			public:
				virtual TextureRef* GetTexture(){return &mRef;}

				TextureRef mRef;
		};

		class SoundContent : public BaseContent
		{
		public:
			virtual WavRef* GetWav(){ return &mRef; }

			WavRef mRef;
		};

		class FontContent : public BaseContent
		{
		public:
			virtual FontRef* GetFont(){return &mRef;}

			FontRef mRef;
		};

		typedef std::map<const char*,BaseContent*> ContentDictionary;
		ContentDictionary TheContentDictionary;

		bool LoadTexture(const char* Name, const char* Filename);
		bool LoadWAV(const char* Name, const char* Filename);

		bool LoadFont(const char* Name,char* FontName, int size, int flags);


		BaseContent* GetContent(const char* name);
		
};