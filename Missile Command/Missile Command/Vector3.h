#pragma once
#include <d3dx9math.h>

class Vector3 : public D3DXVECTOR3
{
	public:
		Vector3(){};
		Vector3(float _x,float _y, float _z)
		{
			x = _x;
			y = _y;
			z = _z;
		};
		
		static Vector3 Transform(const Vector3& in, const Matrix& mat)
		{
			D3DXVECTOR4 out;

			D3DXVec3Transform(&out,&in,&mat);

			return Vector3(out.x,out.y,out.z);
		}

		static float Distance(const Vector3& p1, const Vector3& p2)
		{
			float x = p1.x - p2.x;
			float y = p1.y - p2.y;
			float z = p1.z - p2.z;

			return sqrt((x*x) + (y*y) + (z*z));
		}

		static float Length(const Vector3& in)
		{
			return sqrt((in.x*in.x) + (in.y*in.y) + (in.z*in.z));
		}

		void operator= (const D3DXVECTOR3& vec)
		{
			memcpy(this, vec, sizeof(Vector3));
		}

		static Vector3 Normalize(const Vector3& in, const Vector3& p1, const Vector3& p2)
		{
			Vector3 out = in;

			float distance = Distance(p1, p2);

			out.x = out.x / distance;
			out.y = out.y / distance;
			out.z = out.z / distance;

			return out;
		}

};