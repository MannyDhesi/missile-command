#pragma once

#include "BaseGame.h"

#include "Player.h"

#include "Planet.h"
#include "MissileSatellites.h"
#include "MissilePlayer.h"
#include "MissileEnemy.h"
#include "Explosion.h"

#include "GameManager.h"


class Game : Singleton<Game>
{
	public:
		static Game* Get();

		void Init();
		bool Update();
		void Draw();
		void Exit();

		Player player;

		Planet planet;
		MissileSatellites missileSatellite[4];

		std::list<MissilePlayer> missile_PlayerList;
		std::list<MissileEnemy> missile_EnemyList;
		std::list<Explosion> explosionList;

		struct CUSTOMVERTEX
		{
			Vector3 pos;      // The untransformed, 3D position for the vertex
			DWORD color;        // The vertex color
			Vector2 uv0;
		};

		IDirect3DVertexDeclaration9* vertexDecleration;
};